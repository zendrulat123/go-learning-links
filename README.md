# Go-learning-links

1. [Base Resources](https://docs.google.com/document/d/1Zb9GCWPKeEJ4Dyn2TkT-O3wJ8AFc-IMxZzTugNCjr-8/edit?usp=sharing)

2. [Just starting](https://docs.google.com/document/d/1pu0vRb17zL5Sd4uEQBCG4dMtPy8syqfYH_l_SsghMJM/edit?usp=sharing)

3. [Just Base Local files](https://github.com/golangast/Dashboardtemplate)
    1.   [Git, Mod files, nested structs, local files](https://docs.google.com/document/d/1EN7UOZnC3BHe0-FHSvGk6MXjTY7aTCYRcMbvZ7s_Z1Y/edit?usp=sharing)
    2.   [Mod files, nested structs, Seperate local files, Server, Context](https://github.com/golangast/mod-server-context-local-nestedstructs)
    3.   [Docker/Docker compose](https://github.com/golangast/docker-docker-compose)
    4.   [Database/api are under Handlers folder](https://github.com/golangast/db-api)
    5.   [Go mod, Docker, Docker-compose with two apps](https://gitlab.com/zendrulat123/network/tree/go-mod-docker-docker-compose)

.

5. [Docker](https://docs.google.com/document/d/1lqpv7zRxL1DPmlP2KmcsZd9O1JwgfzRzpK3ZBu9Th2U/edit?usp=sharing)

6. [Containers](https://docs.google.com/document/d/1ZSplcWI9GI7ZgBanBynh68x4oGCASPYJGF7AksYq8m8/edit?usp=sharing)
7. [Testing apis localhost](https://docs.google.com/document/d/1MSqMjk5sT1yY78sosQIL027oUG3a1GngcZd2txZ6WXQ/edit?usp=sharing)


If you are having issues with Modules, remember to use **sudo -s** and set **export GO111MODULE=on** and then
run either go mod tidy 
or 
go mod init projectname

to get zsh [agnoster theme](https://github.com/agnoster/agnoster-zsh-theme) terminal on [linux](https://maxim-danilov.github.io/make-linux-terminal-great-again/) 

